// === seq_loadxy.v =========================================================
//
//    sequencer command
//    load ROC4sens X- or Y-shift register with data
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_selx
(
	input clk,
	input res,
	input stop,

	input tin,
	output reg tout,
	output reg [3:0]cyc
);

	`include "cycles.vh"

	reg [1:0]del;

	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			{tout, del} <= 3'b00;
			cyc <= CYC_WAIT;
		end
		else if (stop)
		begin
			{tout, del} <= 3'b00;
			cyc <= CYC_WAIT;
		end
		else
		begin
			{tout, del} <= {del, tin};
			cyc <= tin ? CYC_DIRX : CYC_WAIT;
		end
	end

endmodule


module seq_sely
(
	input clk,
	input res,
	input stop,

	input tin,
	output reg tout,
	output reg [3:0]cyc
);

	`include "cycles.vh"

	reg [1:0]del;

	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			{tout, del} <= 3'b00;
			cyc <= CYC_WAIT;
		end
		else if (stop)
		begin
			{tout, del} <= 3'b00;
			cyc <= CYC_WAIT;
		end
		else
		begin
			{tout, del} <= {del, tin};
			cyc <= tin ? CYC_DIRY : CYC_WAIT;
		end
	end

endmodule


module seq_loadxy
(
	input clk,
	input res,
	input stop,

	input tin_x,
	input tin_y,
	output tout,
	output [3:0]cyc,
	
	input [154:0]data_x,
	input [159:0]data_y
);

	`include "cycles.vh"

	// --- switch x/y
	wire tok_x;
	wire tok_y;
	wire [3:0]cyc_x;
	wire [3:0]cyc_y;
	
	seq_selx selx(clk, res, stop, tin_x, tok_x, cyc_x);
	seq_sely sely(clk, res, stop, tin_y, tok_y, cyc_y);

	// --- state definition	
	reg [6:0]state;
	localparam STATE_IDLE  = 7'd0;
	localparam STATE_LAST  = 7'd80;

	reg running;
	wire [3:0]_cyc;
	assign tout = (state == STATE_LAST);
	assign cyc  = (running ? _cyc : 4'b0000) | cyc_x | cyc_y;

	always @(posedge clk or posedge res)
	begin
		if (res)			running <= 1'b0;
		else if (stop) running <= 1'b0;
		else if (tok_x || tok_y) running <= 1'b1;
		else if (tout) running <= 1'b0;
	end

	
	// --- input data register (datax/datay -> d)
		
	wire loadx = tok_x;
	wire loady = tok_y;
	wire shift = running;
	
	reg [159:0]dreg;
	always @(posedge clk or posedge res)
	begin
		if (res) dreg <= 160'd0;
		else if (stop) dreg <= 160'd0;
		else
		begin
			if (shift) dreg <= { dreg[157:0], 2'b00 };
			else if (loadx) dreg <= { 5'd0, data_x };
			else if (loady) dreg <= data_y;
		end
	end
	
	wire [1:0]d = dreg[159:158];


	// --- sequence definition
	
	always @(posedge clk or posedge res)
	begin
		if (res) state <= STATE_IDLE;
		else if (stop) state <= STATE_IDLE;
		else
		begin
			if (tok_x || tok_y || (running && !tout)) state <= state + 7'd1;
			else state <= STATE_IDLE;
		end
	
	end

	assign _cyc = {2'b11, d}; // CYC_DSHIFTdd

endmodule
