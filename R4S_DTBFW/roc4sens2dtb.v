// roc4send2dtb.v


module roc4sens2dtb
(
	input clk,
	input reset,
	
	input PHI1,
	input PHI2,
	input SELCLK,
	input RBI,
	input LA,
	input HOLD,
	input VCAL_ENA,
	input VCAL_PULSE,

	output dtb_ctr,        // !PHI1 (LVDS)
	output dtb_clk,        // PHI2 (LVDS)
	output dtb_reset_n,    // SELCLK (Logic)
	output dtb_tin,        // RBI (LVDS)
	output [3:0]dtb_addr,  // LA (pseudo LVDS)
	output dtb_sda,        // HOLD (LVDS)
	output dtb_io3,        // VCAL_ENA (Logic)
	output dtb_io1         // VCAL_PULSE (Logic)
);

	output_reg reg_phi1(clk, reset, !PHI1, dtb_ctr);
	
	output_reg reg_phi2(clk, reset, PHI2, dtb_clk);
	
	output_reg reg_sclk(clk, reset, SELCLK, dtb_reset_n);
	
	output_reg reg_rbi (clk, reset, RBI, dtb_tin);
	
	output_reg reg_la_p(clk, reset, LA, dtb_addr[2]);
	output_reg reg_la_n(clk, reset, !LA, dtb_addr[3]);
	assign dtb_addr[1:0] = 2'bzz;
	
	output_reg reg_hold(clk, reset, !HOLD, dtb_sda);

	output_reg reg_vcalena(clk, reset, VCAL_ENA, dtb_io3);
	
	output_reg reg_vcalpulse(clk, reset, VCAL_PULSE, dtb_io1);
	
endmodule
