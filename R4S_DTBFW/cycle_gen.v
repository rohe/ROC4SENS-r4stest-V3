// === cycle_gen.v ==========================================================
//
//    cycle generator (clock crossing)
//    Beat Meier PSI
//    7/24/2017
//
// ==========================================================================


module cycle_gen
(
	input clk,
	input serclk,
	input res,
	
	input [3:0]cmd,
	output reg [1:0]cycle,
	
	output reg phi1,
	output reg phi2,
	output reg sclk,
	output reg rbi,
	
	input la_in,
	input cal_ena_in,
	input [2:0]cal_pulse_in,  // [2:1] delay, [0] signal
	input [2:0]hold_in, // [2:1] delay, [0] signal
	
	output reg la,
	output reg cal_ena,
	output cal_pulse,
	output hold
);

	// --- sequence counter: clk -> cycle -------------------------------------
	reg [1:0]cycle_shift;
	
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			cycle_shift <= 2'b00;
			cycle       <= 2'b00;
		end
		else
		begin
			cycle_shift <= {cycle_shift[0], clk};
			case (cycle_shift)
				2'b10: cycle <= 2'd0;
				2'b00: cycle <= 2'd1;
				2'b01: cycle <= 2'd2;
				2'b11: cycle <= 2'd3;				
			endcase
		end
	end

	// --- command input register (clock crossing) ----------------------------
	reg [3:0]seq;
	always @(posedge serclk or posedge res)
	begin
		if (res) seq <= 4'b0000;
		else if (cycle == 2'd3) seq <= cmd;		
	end
	
	// --- cycle pattern generator	-------------------------------------------
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			{phi1, phi2, sclk, rbi} <= 4'd0;
		end
		else
		casez (seq)
			4'b001?: // CYC_RES0, CYC_RES1
				begin
					case (cycle)
						2'd0: {phi1, phi2, rbi} <= {2'b11, seq[0]};
					endcase
				end
			4'b01?0: // CYC_DIRX, CYC_DIRY
				begin
					case (cycle)
						2'd0: phi1 <= 1'b0;
						2'd2: sclk <= seq[1];
					endcase
				end		
			4'b100?: // CYC_SHIFT0, CYC_SHIFT1
				begin
					case (cycle)
						2'd0: {phi1, phi2, rbi} <= {2'b10, seq[0]};
						2'd1: {phi1, phi2} <= 2'b01;
					endcase
				end		
			4'b11??: // CYC_DSHIFT00, CYC_DSHIFT01, CYC_DSHIFT10, CYC_DSHIFT11
				begin
					case (cycle)
						2'd0: {phi1, phi2, rbi} <= {2'b10, seq[1]};
						2'd1: {phi1, phi2}      <=  2'b01;
						2'd2: {phi1, phi2, rbi} <= {2'b10, seq[0]};
						2'd3: {phi1, phi2}      <=  2'b01;
					endcase	
				end		
		endcase // seq
	end

	// --- LA -----------------------------------------------------------------
	reg sla;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			sla <= 0;
			la  <= 0;
		end
		else
		begin
			if (cycle == 2'd3) sla <= la_in;
			if (cycle == 2'd0) la  <= sla;
		end
	end

	// --- cal_ena ------------------------------------------------------------
	reg scalena;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			scalena <= 0;
			cal_ena <= 0;
		end
		else
		begin
			if (cycle == 2'd3) scalena <= cal_ena_in;
			if (cycle == 2'd0) cal_ena <= scalena;
		end
	end

	// --- CAL ----------------------------------------------------------------
	cycle_delay cal_cycle(serclk, res, cycle, cal_pulse_in[2:1], cal_pulse_in[0], cal_pulse);

	// --- HOLD ----------------------------------------------------------------
	cycle_delay hold_cycle(serclk, res, cycle, hold_in[2:1], hold_in[0], hold);

endmodule



module cycle_delay
(
	input serclk,
	input	res,
	input [1:0]cycle,	
	input [1:0]delay,
	
	input in,
	output reg out
);
	// --- command input register (clock crossing) ----------------------------
	reg [1:0]sdelay;
	reg sin;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			sdelay <= 2'd0;
			sin    <= 1'd0;
			out    <= 1'b0;
		end
		else
		begin
			if (cycle == 2'd3)
			begin
				sdelay <= delay;
				sin    <= in;
			end
			if (sdelay == cycle) out <= sin;
		end
	end
endmodule
