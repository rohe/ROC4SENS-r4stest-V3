// roc4sens_ctrl_reg.v


module roc4sens_ctrl_reg
(
	input  clk,
	input  reset,
	
	// avs_ctrl
	input   avs_ctrl_read,
	input   avs_ctrl_write,
	input   [8:0]avs_ctrl_address,
	output  [31:0]avs_ctrl_readdata,
	input   [31:0]avs_ctrl_writedata,
	
	// roc4sens interface
	output  roc4sens_read,
	output  roc4sens_write,
	output  [8:0]roc4sens_address,
	input   [31:0]roc4sens_readdata,
	output  [31:0]roc4sens_writedata
);

	assign roc4sens_read  = avs_ctrl_read;
	assign roc4sens_write = avs_ctrl_write;
	assign roc4sens_address   = avs_ctrl_address;
	assign roc4sens_writedata  = avs_ctrl_writedata;
	assign avs_ctrl_readdata = roc4sens_readdata;

endmodule
