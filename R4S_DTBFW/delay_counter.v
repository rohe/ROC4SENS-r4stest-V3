// delay_counter.v


module delay_counter
#(parameter WIDTH=4)
(
	input clk,
	input res,
	
	input [(WIDTH-1):0]repcnt,
	input cntena,
	output next
);
	reg [(WIDTH-1):0]delay;
	assign next = delay == 1;

	wire waitrq = |repcnt;
	wire running;
	assign running = |delay;
	
	always @(posedge clk or posedge res)
	begin
		if (res) delay <= 0;
		else if (running)
		begin
			if (cntena) delay <= delay - 1;
		end
		else if (waitrq)  delay <= repcnt;
	end
endmodule
