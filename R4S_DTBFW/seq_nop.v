// === seq_nop.v ============================================================
//
//    sequencer command
//    no operation (minimal delay needed for sequencer)
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_nop
(
	input clk,
	input res,
	input stop,

	input tin,
	output reg tout
);

	reg [1:0]del;

	always @(posedge clk or posedge res)
	begin
		if (res) {tout, del} <= 3'b00;
		else if (stop) {tout, del} <= 3'b00;
		else {tout, del} <= {del, tin};
	end
	
endmodule
