// === seq_resety.v =========================================================
//
//    sequencer command
//    clear ROC4sens Y shift register
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_resety
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output tout,
	output [3:0]cyc
);

	`include "cycles.vh"

	// --- state definition	
	reg [3:0]state;
	localparam STATE_IDLE  = 4'd0;
	localparam STATE_LAST  = 4'd12;

	reg running;
	reg [3:0]_cyc;
	assign tout = (state >= STATE_LAST);
	assign cyc  = running ? _cyc : 4'b0000;

	always @(posedge clk or posedge res)
	begin
		if (res) running <= 0;
		else if (stop) running <= 0;
		else if (tin)  running <= 1;
		else if (tout) running <= 0;
	end
	
	
	// --- sequence definition
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			_cyc  <= CYC_DIRY;
			state <= STATE_IDLE;
		end
		else if (stop)
		begin
			_cyc  <= CYC_DIRY;
			state <= STATE_IDLE;
		end
		else
		begin
			if (tin || (running && !tout)) state <= state + 4'd1;
			else state <= STATE_IDLE;
		end
		
		case (state)
			 4'd0: _cyc <= CYC_DIRY;
			 4'd2: _cyc <= CYC_RES0;
			4'd11: _cyc <= CYC_DIRX;
			default: _cyc <= CYC_WAIT;
		endcase
	end

endmodule
