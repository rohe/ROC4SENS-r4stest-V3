// === seq_start_stop.v =====================================================
//
//    sequencer command
//    sequencer start/stop implenemted as a command
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_start_stop
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output reg tout,
	
	input start,
	output reg jump0,
	output reg running
);

	reg start1;
	reg [1:0]del;
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			start1  <= 0;
			jump0   <= 0;
			del     <= 2'b00;
			tout    <= 0;
			running <= 0;
		end
		else if (stop)
		begin
			start1  <= 0;
			jump0   <= 0;
			del     <= 2'b00;
			tout    <= 0;
			running <= 0;
		end
		else
		begin
			start1 <= start;

			if (!running) jump0 <= start && !start1;
			{tout, del} <= {del, jump0};

			if (tin) running <= 1'b0;
			else if (tout) running <= 1'b1;
		end
	end

endmodule
