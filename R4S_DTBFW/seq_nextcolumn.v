// === seq_nextcolumn.v =======================================================
//
//    sequencer command
//    select next column (shift right)
//    Aliakbar Ebrahimi DESY
//    14/08/2017
//
// ==========================================================================


module seq_nextcolumn
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output tout,

	output [3:0]cyc
);

	`include "cycles.vh"

	reg running;
	reg [3:0]_cyc;
	
	reg [2:0]state;
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			_cyc   <= CYC_WAIT;
			state <= 3'd0;
		end
		else if (stop)
		begin
			_cyc   <= CYC_WAIT;
			state <= 3'd0;
		end
		else
		begin
			case (state)
				3'd0: if (tin)
					begin
						_cyc <= CYC_DIRX;
						state <= 3'd1;
					end
				3'd1:
					begin
						_cyc <= CYC_WAIT;
						state <= 3'd2;
					end
				3'd2:
					begin
						state <= 3'd3;
					end
				3'd3:
					begin
						_cyc <= CYC_SHIFT0;
						state <= 3'd4;
					end
				3'd4:
					begin
						_cyc <= CYC_WAIT;
						state <= tin ? 3'd1 : 3'd0;
					end
			endcase
		end
	end

	always @(posedge clk or posedge res)
	begin
		if (res) running <= 1'b0;
		else if (stop) running <= 1'b0;
		else if (tin)  running <= 1'b1;
		else if (tout) running <= 1'b0;
	end

	assign tout = (state == 3'd4);
	assign cyc  = running ? _cyc : 4'b0000;

endmodule
