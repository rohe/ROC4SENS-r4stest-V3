// === adc_data.v ===========================================================
//
//    ADC clock/data timing adjust
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module adc_data
(
	input clk,
	input serclk,
	input res,
	input [1:0]cycle,
	
	input davail,
	input [4:0]delay,
	
	output reg adc_clk,
	input [12:0]data_in,
	
	output reg valid,
	output reg [12:0]data_out
);
	//  (falsepath delay -> *)

	// --- coarse adjust: davail -> valid delay
	reg [18:0]del;
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			del <= 19'd0;
			valid <= 1'b0;
		end
		else
		begin
			del <= { del[17:0], davail };
			case (delay[4:2])
				3'd0: valid <= del[11];
				3'd1: valid <= del[12];
				3'd2: valid <= del[13];
				3'd3: valid <= del[14];
				3'd4: valid <= del[15];
				3'd5: valid <= del[16];
				3'd6: valid <= del[17];
				3'd7: valid <= del[18];
			endcase
		end
	end
	

	// --- fine adjust: sampling point
	reg ena1;
	reg ena2;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			ena1 <= 0;
			ena2 <= 0;
		end
		else
		begin
			ena1 <= (cycle == delay[1:0]);
			ena2 <= (cycle == 2'd0);
		end
	end


	// --- data sampling
	reg [12:0]s1;
	reg [12:0]s2;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			s1 <= 13'd0;
			s2 <= 13'd0;
		end
		else
		begin
			if (ena1) s1 <= data_in;
			if (ena2) s2 <= s1;
		end
	end
	
	// data clock crossing
	always @(posedge clk or posedge res)
	begin
		if (res) data_out <= 13'd0;
		else data_out <= s2;
	end
		

	// --- ADC clock output
	reg [1:0]enac;
	always @(posedge serclk or posedge res)
	begin
		if (res)
		begin
			enac <= 2'd0;
			adc_clk <= 1'b0;
		end
		else
		begin
			enac <= {enac[0], ena1};
			if (ena1) adc_clk <= 1'b1;
			else if (enac[1]) adc_clk <= 1'b0;
		end
	end


endmodule
