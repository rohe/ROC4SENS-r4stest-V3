// === seq_extrig.v ========================================================
//
//    sequencer command
//    generate the hold signal on an external trigger
//    Aliakbar Ebrahimi DESY
//    17/08/2017
//
// ==========================================================================


module seq_extrig
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output tout,

	output reg [2:0]hold,

	// parameter
	input [7:0]holdpos,
	input extrig_enable,
	input extrig
);

	reg running;
	reg [2:0]state;
	reg en_counter;
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			state	<= 3'd0;
			en_counter <= 1'd0;
		end
		else if (stop)
		begin
			state	<= 3'd0;
			en_counter <= 1'd0;
		end
		else
		begin
			case (state)
				3'd0:
					if (tin)
						begin
							en_counter <= 1'd0;
							state <= 3'd1;
						end
					else
						begin
						   en_counter <= 1'd0;
							state <= 3'd0;
						end
				3'd1:
					if (extrig_enable)
					begin
						if (extrig)
							begin
								en_counter <= 1'd1;
								state	<= 3'd2;
							end
						else
						begin
							state	<= 3'd1;
						end
					end
				3'd2:
					begin
						en_counter <= 1'd0;
						state <= 3'd0;
					end
			endcase
		end
	end

	reg [7:0] counter;
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			counter <= 8'd0;
			hold	  <= 3'd0;
		end
		else if (stop)
		begin
			counter <= 8'd0;
			hold	<= 3'd0;
		end
		else
		begin
			if (tin)
			begin
			counter  <= 8'd0;
			hold <= 3'd0;
			end
			
			if (en_counter) counter <= 8'd1;
			else if (|counter) counter <= counter + 8'd1;
			
			if (|holdpos[7:2])
			begin
				if (counter == {2'b00, holdpos[7:2]})
				begin
					hold 	<= {holdpos[1:0], 1'b1};
				end
			end
			else
			begin
				if (en_counter)
				begin
					hold 	  <= {holdpos[1:0], 1'b1};
				end
			end
				
		end
	end
	
	assign tout = (counter == holdpos + 8'd1);
	
	always @(posedge clk or posedge res)
	begin
		if (res) running <= 1'b0;
		else if (stop)   running <= 1'b0;
		else if (tin)    running <= 1'b1;
		else if (tout)   running <= 1'b0;
	end
	
endmodule
